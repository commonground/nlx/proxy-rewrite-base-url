# Copy static docs to alpine-based nginx container.
FROM nginx:alpine
EXPOSE 8080

# Add non-privileged user
RUN adduser -D -u 1001 appuser

# Set ownership nginx.pid and cache folder in order to run nginx as non-root user
RUN touch /var/run/nginx.pid && \
    chown -R appuser /var/run/nginx.pid && \
    chown -R appuser /var/cache/nginx

# Copy nginx configuration
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/default.conf /etc/nginx/conf.d/default.conf

RUN chown -R appuser /etc/nginx/conf.d/

RUN apk add --no-cache sed bash

COPY setup.sh /startup/setup.sh
RUN chmod +x /startup/setup.sh

# This is a workaround to https://github.com/moby/moby/issues/37965
RUN true

USER appuser

CMD /bin/bash /startup/setup.sh && nginx -g 'daemon off;'
