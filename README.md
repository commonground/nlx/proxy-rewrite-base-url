# proxy-rewrite-base-url

Build image:

```shell
docker build -t my-proxy .
```

Run image:

```shell
docker run \
  -e REPLACE_BASE_URL_FROM=<old-base-domain> \
  -e REPLACE_BASE_URL_TO=<new-base-url> \
  -e PROXY_PASS=<outway-address> \
  my-proxy
```

Example values as reference:

```shell
REPLACE_BASE_URL_FROM=https://demo.ldproxy.net/daraa
REPLACE_BASE_URL_TO=https://nlx-madrid-proxy-order-vgs-bv-{{DOMAIN_SUFFIX}}
PROXY_PASS=https://nlx-outway-order-vgs-{{DOMAIN_SUFFIX}}/12345678901234567891/daraa/
```
