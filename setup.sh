#!/bin/bash
if [[ -z "${REPLACE_BASE_URL_FROM}" ]]; then
  echo "REPLACE_BASE_URL_FROM env is required"
  exit 1
fi

if [[ -z "${REPLACE_BASE_URL_TO}" ]]; then
  echo "REPLACE_BASE_URL_TO env is required"
  exit 1
fi

if [[ -z "${PROXY_PASS}" ]]; then
  echo "PROXY_PASS env is required"
  exit 1
fi

sed -i -e "s/{{ REPLACE_BASE_URL_FROM }}/${REPLACE_BASE_URL_FROM//\//\\/}/g" /etc/nginx/conf.d/default.conf
sed -i -e "s/{{ REPLACE_BASE_URL_TO }}/${REPLACE_BASE_URL_TO//\//\\/}/g" /etc/nginx/conf.d/default.conf
sed -i -e "s/{{ PROXY_PASS }}/${PROXY_PASS//\//\\/}/g" /etc/nginx/conf.d/default.conf

echo "SETUP OK"
exit 0
